系统笔记-实时云日志分析项目

项目官网:[系统笔记云日志系统](http://www.logstack.cn)
1.发送任意的文本日志到系统笔记,此进程不依赖与系统本身的syslog,可以单独使用
请下载  send_to_logstack.tar.gz

需要安装ruby 2.1以上版本
执行完ruby安装
cd gem
gem install sent_to_logstack.gem -l


2.安装启动脚本
下载send_to_logstack.init.d 到本地
cp send_to_logstack.init.d /etc/init.d/send_to_logstack

3.添加配置文件:
vi /etc/send_file.yml

4.配置文件示例:
"[]"内为您的uuid,注意最后必须有一个空格!

prepend: "[f19485ae-90dd-908e-5c0a-6df83b8f1037] "
files:
 - /var/log/httpd/access_log
 - /var/log/httpd/error_log
 - /var/log/mysqld.log
 - /var/run/mysqld/mysqld-slow.log
destination:
  host: server1.logstack.cn
  port: 514


5.启动日志发送守护
/etc/init.d/send_to_logstack start