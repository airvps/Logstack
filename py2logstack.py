import logging
from logging.handlers import SysLogHandler

syslog = logging.getLogger('python')
syslog.setLevel(logging.DEBUG)

format='%(name)s[%(process)d] %(levelname)s %(message)s'
f = logging.Formatter(format)
h = SysLogHandler(address=('server1.logstack.cn',514), facility=SysLogHandler.LOG_LOCAL6)
h.ident = 'conmon'

h.setFormatter(f)
syslog.addHandler(h)

def debug(message):
        syslog.debug('message')
